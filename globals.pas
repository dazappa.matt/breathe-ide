unit globals;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ComCtrls, dialogs;

resourcestring
  SSpriteProperties = 'Sprite Properties';
  SSoundProperties = 'Sound Properties';
  SBackgroundProperties = 'Background Properties';
  SPathProperties = 'Path Properties';
  SScriptProperties = 'Script Properties';
  SShaderProperties = 'Shader Properties';
  SFontProperties = 'Font Properties';
  STimelineProperties = 'Time Line Properties';
  SObjectProperties = 'Object Properties';
  SRoomProperties = 'Room Properties';
  SWidth = 'Width';
  SHeight = 'Height';
  SImageLoadErr = 'Unable to load the image.';
  SResourceAlreadyExists = 'A resource with this name already exists.';

const
  // Indexes of resources in the tree view
  TV_SPRITES: integer = 0;
  TV_SOUNDS: integer = 1;
  TV_BACKGROUNDS: integer = 2;
  TV_PATHS: integer = 3;
  TV_SCRIPTS: integer = 4;
  TV_SHADERS: integer = 5;
  TV_FONTS: integer = 6;
  TV_TIME_LINES: integer = 7;
  TV_OBJECTS: integer = 8;
  TV_ROOMS: integer = 9;
  //TV_GAME_INFO: integer = 10;
  TV_GGS: integer = 10;
  TV_CONSTANTS: integer = 11;

type
  pSprite = ^TBSprite;
  TBSprite = record
    name: string;
    node: TTreeNode;
    formPos: TRect;
    origin: TPoint;
    usePreciseCollisions: boolean;
    // TODO: mask, image data.
  end;

  pSound = ^TBSound;
  TBSound = record
    name: string;
    node: TTreeNode;
    formPos: TRect;
  end;

  pBackground = ^TBBackground;
  TBBackground = record
    name: string;
    node: TTreeNode;
    formPos: TRect;
  end;

  pPath = ^TBPath;
  TBPath = record
    name: string;
    node: TTreeNode;
    formPos: TRect;
  end;

  pScript = ^TBScript;
  TBScript = record
    name: string;
    node: TTreeNode;
    formPos: TRect;
  end;

  pShader = ^TBShader;
  TBShader = record
    name: string;
    node: TTreeNode;
    formPos: TRect;
  end;

  pFont = ^TBFont;
  TBFont = record
    name: string;
    node: TTreeNode;
    formPos: TRect;
  end;

  pTimeLine = ^TBTimeLine;
  TBTimeLine = record
    name: string;
    node: TTreeNode;
    formPos: TRect;
  end;

  pObject = ^TBObject;
  TBObject = record
    name: string;
    node: TTreeNode;
    formPos: TRect;
    sprite: integer; // TODO: sprite type
    visible: boolean;
    solid: boolean;
    persistent: boolean;
    usesPhysics: boolean;
    depth: integer;
    parent: pObject;
    mask: integer; // TODO: sprite type
  end;

  pRoom = ^TBRoom;
  TBRoom = record
    name: string;
    node: TTreeNode;
    formPos: TRect;
  end;

var
  // TTreeNodes of level 0 items (resources etc.)
  TN_SPRITES: TTreeNode;
  TN_SOUNDS: TTreeNode;
  TN_BACKGROUNDS: TTreeNode;
  TN_PATHS: TTreeNode;
  TN_SCRIPTS: TTreeNode;
  TN_SHADERS: TTreeNode;
  TN_FONTS: TTreeNode;
  TN_TIME_LINES: TTreeNode;
  TN_OBJECTS: TTreeNode;
  TN_ROOMS: TTreeNode;
  TN_GAME_INFO: TTreeNode;
  TN_GGS: TTreeNode;
  TN_CONSTANTS: TTreeNode;
  // Actual resource data
  pSprites: array of pSprite;
  pSounds: array of pSound;
  pBackgrounds: array of pBackground;
  pPaths: array of pPath;
  pScripts: array of pScript;
  pShaders: array of pShader;
  pFonts: array of pFont;
  pTimeLines: array of pTimeLine;
  pObjects: array of pObject;
  pRooms: array of pRoom;

procedure AddSprite(node: TTreeNode);
procedure AddSound(node: TTreeNode);
procedure AddBackground(node: TTreeNode);
procedure AddPath(node: TTreeNode);
procedure AddScript(node: TTreeNode);
procedure AddShader(node: TTreeNode);
procedure AddFont(node: TTreeNode);
procedure AddTimeLine(node: TTreeNode);
procedure AddObject(node: TTreeNode);
procedure AddRoom(node: TTreeNode);

function IsResourceNameUnique(name: string; ignore: TTreeNode): boolean;
function SafeResourceName(name: string): string;

function GetSpriteFromName(name: string): pSprite;
function GetSoundFromName(name: string): pSound;
function GetBackgroundFromName(name: string): pBackground;
function GetPathFromName(name: string): pPath;
function GetScriptFromName(name: string): pScript;
function GetShaderFromName(name: string): pShader;
function GetFontFromName(name: string): pFont;
function GetTimeLineFromName(name: string): pTimeLine;
function GetObjectFromName(name: string): pObject;
function GetRoomFromName(name: string): pRoom;

implementation

procedure AddSprite(node: TTreeNode);
var
  len: integer;
begin
  len := Length(pSprites);
  SetLength(pSprites, len+1);
  new(pSprites[len]);
  pSprites[len]^.node := node;
  pSprites[len]^.name := node.Text;
  pSprites[len]^.formPos.Left := -1;
  pSprites[len]^.formPos.Top := -1;
  pSprites[len]^.formPos.Right := -1;
  pSprites[len]^.formPos.Bottom := -1;

  pSprites[len]^.usePreciseCollisions := false;
  pSprites[len]^.origin.x := 0;
  pSprites[len]^.origin.y := 0;
end;

procedure AddSound(node: TTreeNode);
var
  len: integer;
begin
  len := Length(pSounds);
  SetLength(pSounds, len+1);
  new(pSounds[len]);
  pSounds[len]^.node := node;
  pSounds[len]^.name := node.Text;
  pSounds[len]^.formPos.Left := -1;
  pSounds[len]^.formPos.Top := -1;
  pSounds[len]^.formPos.Right := -1;
  pSounds[len]^.formPos.Bottom := -1;
end;

procedure AddBackground(node: TTreeNode);
var
  len: integer;
begin
  len := Length(pBackgrounds);
  SetLength(pBackgrounds, len+1);
  new(pBackgrounds[len]);
  pBackgrounds[len]^.node := node;
  pBackgrounds[len]^.name := node.Text;
  pBackgrounds[len]^.formPos.Left := -1;
  pBackgrounds[len]^.formPos.Top := -1;
  pBackgrounds[len]^.formPos.Right := -1;
  pBackgrounds[len]^.formPos.Bottom := -1;
end;

procedure AddPath(node: TTreeNode);
var
  len: integer;
begin
  len := Length(pPaths);
  SetLength(pPaths, len+1);
  new(pPaths[len]);
  pPaths[len]^.node := node;
  pPaths[len]^.name := node.Text;
  pPaths[len]^.formPos.Left := -1;
  pPaths[len]^.formPos.Top := -1;
  pPaths[len]^.formPos.Right := -1;
  pPaths[len]^.formPos.Bottom := -1;
end;

procedure AddScript(node: TTreeNode);
var
  len: integer;
begin
  len := Length(pScripts);
  SetLength(pScripts, len+1);
  new(pScripts[len]);
  pScripts[len]^.node := node;
  pScripts[len]^.name := node.Text;
  pScripts[len]^.formPos.Left := -1;
  pScripts[len]^.formPos.Top := -1;
  pScripts[len]^.formPos.Right := -1;
  pScripts[len]^.formPos.Bottom := -1;
end;

procedure AddShader(node: TTreeNode);
var
  len: integer;
begin
  len := Length(pShaders);
  SetLength(pShaders, len+1);
  new(pShaders[len]);
  pShaders[len]^.node := node;
  pShaders[len]^.name := node.Text;
  pShaders[len]^.formPos.Left := -1;
  pShaders[len]^.formPos.Top := -1;
  pShaders[len]^.formPos.Right := -1;
  pShaders[len]^.formPos.Bottom := -1;
end;

procedure AddFont(node: TTreeNode);
var
  len: integer;
begin
  len := Length(pFonts);
  SetLength(pFonts, len+1);
  new(pFonts[len]);
  pFonts[len]^.node := node;
  pFonts[len]^.name := node.Text;
  pFonts[len]^.formPos.Left := -1;
  pFonts[len]^.formPos.Top := -1;
  pFonts[len]^.formPos.Right := -1;
  pFonts[len]^.formPos.Bottom := -1;
end;

procedure AddTimeLine(node: TTreeNode);
var
  len: integer;
begin
  len := Length(pTimeLines);
  SetLength(pTimeLines, len+1);
  new(pTimeLines[len]);
  pTimeLines[len]^.node := node;
  pTimeLines[len]^.name := node.Text;
  pTimeLines[len]^.formPos.Left := -1;
  pTimeLines[len]^.formPos.Top := -1;
  pTimeLines[len]^.formPos.Right := -1;
  pTimeLines[len]^.formPos.Bottom := -1;
end;
procedure AddObject(node: TTreeNode);
var
  len: integer;
begin
  len := Length(pObjects);
  SetLength(pObjects, len+1);
  new(pObjects[len]);
  pObjects[len]^.node := node;
  pObjects[len]^.name := node.Text;
  pObjects[len]^.formPos.Left := -1;
  pObjects[len]^.formPos.Top := -1;
  pObjects[len]^.formPos.Right := -1;
  pObjects[len]^.formPos.Bottom := -1;
end;

procedure AddRoom(node: TTreeNode);
var
  len: integer;
begin
  len := Length(pRooms);
  SetLength(pRooms, len+1);
  new(pRooms[len]);
  pRooms[len]^.node := node;
  pRooms[len]^.name := node.Text;
  pRooms[len]^.formPos.Left := -1;
  pRooms[len]^.formPos.Top := -1;
  pRooms[len]^.formPos.Right := -1;
  pRooms[len]^.formPos.Bottom := -1;
end;

// Convert a string to a safe name for a resource
// (A-Z, a-z, 0-9, _)
// Any other characters are changed to underscores
function SafeResourceName(name: string): string;
var
  c: char;
  i, len: Integer;
  valid: boolean;
begin
  // Pas strings are 1-based
  i := 1;
  len := Length(name);

  repeat
    c := name[i];
    valid := false;
    case c of
      '0'..'9': valid := true;
      'a'..'z': valid := true;
      'A'..'Z': valid := true;
      '_': valid := true;
    end;
    if not(valid) then
      name[i] := '_';
    Inc(i);
  until i > len;
  Result := name;
end;

// Check if any resource has the given name.
// n: name
// ignore: TreeNode to ignore matches for
function IsResourceNameUnique(name: string; ignore: TTreeNode): boolean;
var
  len, i: integer;
begin
  Result := true;

  len := Length(pSprites);
  i := 0;
  while i < len do
  begin
    if not(pSprites[i]^.node = ignore) then
    begin
      if pSprites[i]^.name = name then
      begin
        Result := false;
        exit;
        i := len;
      end;
    end;
    Inc(i);
  end;

  len := Length(pSounds);
  i := 0;
  while i < len do
  begin
    if not(pSounds[i]^.node = ignore) then
    begin
      if pSounds[i]^.name = name then
      begin
        Result := false;
        exit;
        i := len;
      end;
    end;
    Inc(i);
  end;

  len := Length(pBackgrounds);
  i := 0;
  while i < len do
  begin
    if not(pBackgrounds[i]^.node = ignore) then
    begin
      if pBackgrounds[i]^.name = name then
      begin
        Result := false;
        exit;
        i := len;
      end;
    end;
    Inc(i);
  end;

  len := Length(pPaths);
  i := 0;
  while i < len do
  begin
    if not(pPaths[i]^.node = ignore) then
    begin
      if pPaths[i]^.name = name then
      begin
        Result := false;
        exit;
        i := len;
      end;
    end;
    Inc(i);
  end;

  len := Length(pScripts);
  i := 0;
  while i < len do
  begin
    if not(pScripts[i]^.node = ignore) then
    begin
      if pScripts[i]^.name = name then
      begin
        Result := false;
        exit;
        i := len;
      end;
    end;
    Inc(i);
  end;

  len := Length(pShaders);
  i := 0;
  while i < len do
  begin
    if not(pShaders[i]^.node = ignore) then
    begin
      if pShaders[i]^.name = name then
      begin
        Result := false;
        exit;
        i := len;
      end;
    end;
    Inc(i);
  end;

  len := Length(pFonts);
  i := 0;
  while i < len do
  begin
    if not(pFonts[i]^.node = ignore) then
    begin
      if pFonts[i]^.name = name then
      begin
        Result := false;
        exit;
        i := len;
      end;
    end;
    Inc(i);
  end;

  len := Length(pTimeLines);
  i := 0;
  while i < len do
  begin
    if not(pTimeLines[i]^.node = ignore) then
    begin
      if pTimeLines[i]^.name = name then
      begin
        Result := false;
        exit;
        i := len;
      end;
    end;
    Inc(i);
  end;

  len := Length(pObjects);
  i := 0;
  while i < len do
  begin
    if not(pObjects[i]^.node = ignore) then
    begin
      if pObjects[i]^.name = name then
      begin
        Result := false;
        exit;
        i := len;
      end;
    end;
    Inc(i);
  end;

  len := Length(pRooms);
  i := 0;
  while i < len do
  begin
    if not(pRooms[i]^.node = ignore) then
    begin
      if pRooms[i]^.name = name then
      begin
        Result := false;
        exit;
        i := len;
      end;
    end;
    Inc(i);
  end;
end;

function GetSpriteFromName(name: string): pSprite;
var
  len, i: integer;
begin
  Result := nil;
  len := Length(pSprites);
  i := 0;
  while i < len do
  begin
    if pSprites[i]^.name = name then
    begin
      Result := pSprites[i];
      i := len;
    end;
    Inc(i);
  end;
end;

function GetSoundFromName(name: string): pSound;
var
  len, i: integer;
begin
  Result := nil;
  len := Length(pSounds);
  i := 0;
  while i < len do
  begin
    if pSounds[i]^.name = name then
    begin
      Result := pSounds[i];
      i := len;
    end;
    Inc(i);
  end;
end;

function GetBackgroundFromName(name: string): pBackground;
var
  len, i: integer;
begin
  Result := nil;
  len := Length(pBackgrounds);
  i := 0;
  while i < len do
  begin
    if pBackgrounds[i]^.name = name then
    begin
      Result := pBackgrounds[i];
      i := len;
    end;
    Inc(i);
  end;
end;

function GetPathFromName(name: string): pPath;
var
  len, i: integer;
begin
  Result := nil;
  len := Length(pPaths);
  i := 0;
  while i < len do
  begin
    if pPaths[i]^.name = name then
    begin
      Result := pPaths[i];
      i := len;
    end;
    Inc(i);
  end;
end;

function GetScriptFromName(name: string): pScript;
var
  len, i: integer;
begin
  Result := nil;
  len := Length(pScripts);
  i := 0;
  while i < len do
  begin
    if pScripts[i]^.name = name then
    begin
      Result := pScripts[i];
      i := len;
    end;
    Inc(i);
  end;
end;

function GetShaderFromName(name: string): pShader;
var
  len, i: integer;
begin
  Result := nil;
  len := Length(pShaders);
  i := 0;
  while i < len do
  begin
    if pShaders[i]^.name = name then
    begin
      Result := pShaders[i];
      i := len;
    end;
    Inc(i);
  end;
end;

function GetFontFromName(name: string): pFont;
var
  len, i: integer;
begin
  Result := nil;
  len := Length(pFonts);
  i := 0;
  while i < len do
  begin
    if pFonts[i]^.name = name then
    begin
      Result := pFonts[i];
      i := len;
    end;
    Inc(i);
  end;
end;

function GetTimeLineFromName(name: string): pTimeLine;
var
  len, i: integer;
begin
  Result := nil;
  len := Length(pTimeLines);
  i := 0;
  while i < len do
  begin
    if pTimeLines[i]^.name = name then
    begin
      Result := pTimeLines[i];
      i := len;
    end;
    Inc(i);
  end;
end;

function GetObjectFromName(name: string): pObject;
var
  len, i: integer;
begin
  Result := nil;
  len := Length(pObjects);
  i := 0;
  while i < len do
  begin
    if pObjects[i]^.name = name then
    begin
      Result := pObjects[i];
      i := len;
    end;
    Inc(i);
  end;
end;

function GetRoomFromName(name: string): pRoom;
var
  len, i: integer;
begin
  Result := nil;
  len := Length(pRooms);
  i := 0;
  while i < len do
  begin
    if pRooms[i]^.name = name then
    begin
      Result := pRooms[i];
      i := len;
    end;
    Inc(i);
  end;
end;

end.

