unit bscript;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  StdCtrls, globals;

type

  { TfrmScript }

  TfrmScript = class(TForm)
    lblName: TLabel;
    txtName: TEdit;
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure UpdateTitle();
    procedure txtNameChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    node: TTreeNode;
    originalName: string;
  end;

implementation

{$R *.lfm}

{ TfrmScript }
procedure TfrmScript.UpdateTitle();
begin
  Caption := SScriptProperties + ': ' + txtName.Text;
end;

procedure TfrmScript.FormCreate(Sender: TObject);
begin

end;

procedure TfrmScript.FormShow(Sender: TObject);
var
  myScript: pScript;
begin
  myScript := GetScriptFromName(originalName);
  // -1, -1 is the default value, if we have it don't override our default position
  if not(myScript^.formPos.Left = -1) and not(myScript^.formPos.Top = -1) and 
     not(myScript^.formPos.Right = -1) and not(myScript^.formPos.Bottom = -1) then
  begin
    Left := myScript^.formPos.Left;
    Top := myScript^.formPos.Top;
    Right := myScript^.formPos.Right;
    Bottom := myScript^.formPos.Bottom;
  end;
end;

procedure TfrmScript.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  myScript: pScript;
begin
  myScript := GetScriptFromName(originalName);
  myScript^.formPos.Left := Left;
  myScript^.formPos.Top := Top;
  myScript^.formPos.Right := Right;
  myScript^.formPos.Bottom := bottom;
  CanClose := true;
end;

procedure TfrmScript.txtNameChange(Sender: TObject);
begin
  UpdateTitle;
end;

end.

