unit btimeline;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  StdCtrls, globals;

type

  { TfrmTimeLine }

  TfrmTimeLine = class(TForm)
    lblName: TLabel;
    txtName: TEdit;
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormShow(Sender: TObject);
    procedure UpdateTitle();
    procedure txtNameChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    node: TTreeNode;
    originalName: string;
  end;

implementation

{$R *.lfm}

{ TfrmTimeLine }
procedure TfrmTimeLine.UpdateTitle();
begin
  Caption := STimeLineProperties + ': ' + txtName.Text;
end;

procedure TfrmTimeLine.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  myTimeLine: pTimeLine;
begin
  myTimeLine := GetTimeLineFromName(originalName);
  myTimeLine^.formPos.Left := Left;
  myTimeLine^.formPos.Top := Top;
  myTimeLine^.formPos.Right := Right;
  myTimeLine^.formPos.Bottom := bottom;
  CanClose := true;
end;

procedure TfrmTimeLine.FormShow(Sender: TObject);
var
  myTimeLine: pTimeLine;
begin
  myTimeLine := GetTimeLineFromName(originalName);
  // -1, -1 is the default value, if we have it don't override our default position
  if not(myTimeLine^.formPos.Left = -1) and not(myTimeLine^.formPos.Top = -1) and 
     not(myTimeLine^.formPos.Right = -1) and not(myTimeLine^.formPos.Bottom = -1) then
  begin
    Left := myTimeLine^.formPos.Left;
    Top := myTimeLine^.formPos.Top;
    Right := myTimeLine^.formPos.Right;
    Bottom := myTimeLine^.formPos.Bottom;
  end;
end;

procedure TfrmTimeLine.txtNameChange(Sender: TObject);
begin
  UpdateTitle;
end;

end.

