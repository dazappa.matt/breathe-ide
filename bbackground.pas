unit bbackground;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  StdCtrls, Buttons, globals;

type

  { TfrmBackground }

  TfrmBackground = class(TForm)
    btnOk: TBitBtn;
    btnEditBackground: TBitBtn;
    btnLoad: TBitBtn;
    chkTileSet: TCheckBox;
    lblDimensions: TLabel;
    lblName: TLabel;
    txtName: TEdit;
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormShow(Sender: TObject);
    procedure UpdateTitle();
    procedure txtNameChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    node: TTreeNode;
    originalName: string;
  end;

implementation

{$R *.lfm}

{ TfrmBackground }
procedure TfrmBackground.UpdateTitle();
begin
  Caption := SBackgroundProperties + ': ' + txtName.Text;
end;

procedure TfrmBackground.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  myBackground: pBackground;
begin
  myBackground := GetBackgroundFromName(originalName);
  myBackground^.formPos.Left := Left;
  myBackground^.formPos.Top := Top;
  myBackground^.formPos.Right := Right;
  myBackground^.formPos.Bottom := bottom;
  CanClose := true;
end;

procedure TfrmBackground.FormShow(Sender: TObject);
var
  myBackground: pBackground;
begin
  myBackground := GetBackgroundFromName(originalName);
  // -1, -1 is the default value, if we have it don't override our default position
  if not(myBackground^.formPos.Left = -1) and not(myBackground^.formPos.Top = -1) and 
     not(myBackground^.formPos.Right = -1) and not(myBackground^.formPos.Bottom = -1) then
  begin
    Left := myBackground^.formPos.Left;
    Top := myBackground^.formPos.Top;
    Right := myBackground^.formPos.Right;
    Bottom := myBackground^.formPos.Bottom;
  end;
end;

procedure TfrmBackground.txtNameChange(Sender: TObject);
begin
  UpdateTitle;
end;

end.

