unit floading;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ComCtrls;

type

  { TfrmLoading }

  TfrmLoading = class(TForm)
    lblStatus: TLabel;
    pbMain: TProgressBar;
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    IgnoreCloseQuery: boolean;
    aborting: boolean;
  end;

var
  frmLoading: TfrmLoading;

implementation


{$R *.lfm}

{ TfrmLoading }

procedure TfrmLoading.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  if IgnoreCloseQuery then
  begin
    CanClose := true;
  end else
  begin
    case QuestionDlg('Abort?', 'Abort loading image?', mtConfirmation, [mrYes,'Yes',mrNo,'No'], 0) of
      mrYes: begin
        aborting := true;
        lblStatus.Caption := 'Aborting...';
      end;
    end;
    CanClose := false;
  end;
end;

procedure TfrmLoading.FormCreate(Sender: TObject);
begin

end;

end.

