program breathe;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, editor, bobject, fconstants, globals, bsprite, about, enigma,
  imageloader, floading, bsound, bbackground, bpath, bscript, bshader, bfont,
  btimeline, broom
  { you can add units after this };

{$R *.res}

begin
  Application.Scaled:=True;
  Application.Title:='Breathe';
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(TfrmEditor, frmEditor);
  Application.CreateForm(TfrmAbout, frmAbout);
  Application.CreateForm(TfrmConstants, frmConstants);
  Application.Run;
end.

