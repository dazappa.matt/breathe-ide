unit bfont;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  StdCtrls, Buttons, globals;

type

  { TfrmFont }

  TfrmFont = class(TForm)
    btnOk: TBitBtn;
    lblName: TLabel;
    txtName: TEdit;
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure UpdateTitle();
    procedure txtNameChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    node: TTreeNode;
    originalName: string;
  end;

implementation

{$R *.lfm}

{ TfrmFont }
procedure TfrmFont.UpdateTitle();
begin
  Caption := SFontProperties + ': ' + txtName.Text;
end;

procedure TfrmFont.FormCreate(Sender: TObject);
begin

end;

procedure TfrmFont.FormShow(Sender: TObject);
var
  myFont: pFont;
begin
  myFont := GetFontFromName(originalName);
  // -1, -1 is the default value, if we have it don't override our default position
  if not(myFont^.formPos.Left = -1) and not(myFont^.formPos.Top = -1) and 
     not(myFont^.formPos.Right = -1) and not(myFont^.formPos.Bottom = -1) then
  begin
    Left := myFont^.formPos.Left;
    Top := myFont^.formPos.Top;
    Right := myFont^.formPos.Right;
    Bottom := myFont^.formPos.Bottom;
  end;
end;

procedure TfrmFont.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  myFont: pFont;
begin
  myFont := GetFontFromName(originalName);
  myFont^.formPos.Left := Left;
  myFont^.formPos.Top := Top;
  myFont^.formPos.Right := Right;
  myFont^.formPos.Bottom := bottom;
  CanClose := true;
end;

procedure TfrmFont.txtNameChange(Sender: TObject);
begin
  UpdateTitle;
end;

end.

