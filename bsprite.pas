unit bsprite;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Buttons, ComCtrls, FPImage, FPReadPNG, globals;

type

  { TfrmSprite }

  TfrmSprite = class(TForm)
    btnLoadSprite: TBitBtn;
    btnEditSprite: TBitBtn;
    btnModifyMask: TBitBtn;
    btnOK: TBitBtn;
    btnCenter: TButton;
    chkPrecise: TCheckBox;
    dlgOpenImage: TOpenDialog;
    sbImage: TScrollBox;
    txtName: TEdit;
    txtX: TEdit;
    txtY: TEdit;
    grpCollision: TGroupBox;
    grpOrigin: TGroupBox;
    imgSprite: TImage;
    lblName: TLabel;
    lblX: TLabel;
    lblY: TLabel;
    lblDimensions: TLabel;
    lblSubimages: TLabel;
    pnLeft: TPanel;
    procedure btnCenterClick(Sender: TObject);
    procedure btnLoadSpriteClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure txtNameChange(Sender: TObject);
    procedure UpdateTitle();
  private
    { private declarations }
  public
    { public declarations }
    node: TTreeNode;
    originalName: string;
  end;

implementation

{$R *.lfm}

{ TfrmSprite }
procedure TfrmSprite.UpdateTitle();
begin
  Caption := SSpriteProperties + ': ' + txtName.Text;
end;

procedure TfrmSprite.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  mySprite: pSprite;
begin
  mySprite := GetSpriteFromName(originalName);
  mySprite^.formPos.Left := Left;
  mySprite^.formPos.Top := Top;
  mySprite^.formPos.Right := Right;
  mySprite^.formPos.Bottom := bottom;
  CanClose := true;
end;

procedure TfrmSprite.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  // TODO: I need the below not commented to keep focus in the child window
  // however, the code below leaves the underlines that alt shows.
  {if Key = 18 then
    Key := 0; }
end;

procedure TfrmSprite.FormShow(Sender: TObject);
var
  mySprite: pSprite;
begin
  mySprite := GetSpriteFromName(originalName);
  // -1, -1 is the default value, if we have it don't override our default position
  if not(mySprite^.formPos.Left = -1) and not(mySprite^.formPos.Top = -1) and 
     not(mySprite^.formPos.Right = -1) and not(mySprite^.formPos.Bottom = -1) then
  begin
    Left := mySprite^.formPos.Left;
    Top := mySprite^.formPos.Top;
    Right := mySprite^.formPos.Right;
    Bottom := mySprite^.formPos.Bottom;
  end;
end;

procedure TfrmSprite.txtNameChange(Sender: TObject);
begin
  UpdateTitle;
end;

procedure TfrmSprite.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  //CloseAction := caFree;
end;

procedure TfrmSprite.btnLoadSpriteClick(Sender: TObject);
begin
  if dlgOpenImage.Execute then
  begin
    try
      imgSprite.Picture.LoadFromFile(dlgOpenImage.FileName);
      imgSprite.Width := imgSprite.Picture.Width;
      imgSprite.Height := imgSprite.Picture.Height;
      lblDimensions.Caption := SWidth + ': '+IntToStr(imgSprite.Width)+', ' + SHeight + ': '+IntToStr(imgSprite.Height);
    except
      // TODO: proper error
      ShowMessage(SImageLoadErr);
    end;
  end;
end;

procedure TfrmSprite.btnOKClick(Sender: TObject);
var
  safeName: string;
  mySprite: pSprite;
begin
  safeName := SafeResourceName(txtName.Text);
  if IsResourceNameUnique(safeName, node) then
  begin
    // Update the data by finding the sprite with the original name
    mySprite := GetSpriteFromName(originalName);
    // Update the data
    mySprite^.name := safeName;
    originalName := safeName;
    mySprite^.origin.x := StrToInt(txtX.Text);
    mySprite^.origin.y := StrToInt(txtY.Text);
    mySprite^.usePreciseCollisions := chkPrecise.Checked;

    node.Text := safeName;
    self.Close;
  end else
  begin
    // TODO: proper error
    ShowMessage(SResourceAlreadyExists);
    txtName.Text := originalName;
  end;
end;

procedure TfrmSprite.btnCenterClick(Sender: TObject);
var
  ox, oy: Integer;
begin
  ox := imgSprite.Width div 2;
  oy := imgSprite.Height div 2;
  txtX.Text := IntToStr(ox);
  txtY.Text := IntToStr(oy);
end;

end.

