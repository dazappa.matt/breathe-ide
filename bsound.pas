unit bsound;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, ComCtrls, globals;

type

  { TfrmSound }

  TfrmSound = class(TForm)
    btnOK: TBitBtn;
    chkPreload: TCheckBox;
    chkCompressed: TCheckBox;
    chkStreamed: TCheckBox;
    chkDecompressOnLoad: TCheckBox;
    cbSoundType: TComboBox;
    txtName: TEdit;
    lblName: TLabel;
    lblVolume: TLabel;
    lblPan: TLabel;
    lblSoundType: TLabel;
    btnOpen: TSpeedButton;
    btnPlay: TSpeedButton;
    btnStop: TSpeedButton;
    tbVolume: TTrackBar;
    tbPan: TTrackBar;
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormShow(Sender: TObject);
    procedure UpdateTitle();
    procedure txtNameChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    node: TTreeNode;
    originalName: string;
  end;

implementation

{$R *.lfm}

{ TfrmSound }
procedure TfrmSound.UpdateTitle();
begin
  Caption := SSoundProperties + ': ' + txtName.Text;
end;

procedure TfrmSound.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  mySound: pSound;
begin
  mySound := GetSoundFromName(originalName);
  mySound^.formPos.Left := Left;
  mySound^.formPos.Top := Top;
  mySound^.formPos.Right := Right;
  mySound^.formPos.Bottom := bottom;
  CanClose := true;
end;

procedure TfrmSound.FormShow(Sender: TObject);
var
  mySound: pSound;
begin
  mySound := GetSoundFromName(originalName);
  // -1, -1 is the default value, if we have it don't override our default position
  if not(mySound^.formPos.Left = -1) and not(mySound^.formPos.Top = -1) and 
     not(mySound^.formPos.Right = -1) and not(mySound^.formPos.Bottom = -1) then
  begin
    Left := mySound^.formPos.Left;
    Top := mySound^.formPos.Top;
    Right := mySound^.formPos.Right;
    Bottom := mySound^.formPos.Bottom;
  end;
end;

procedure TfrmSound.txtNameChange(Sender: TObject);
begin
  UpdateTitle;
end;

end.

