unit editor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, PairSplitter,
  ComCtrls, Menus, ExtCtrls, StdCtrls, globals, fconstants,
  bsprite, bsound, bbackground, bpath, bscript, bshader, bfont, btimeline, bobject, broom,
  about, enigma;

type

  { TfrmEditor }

  TfrmEditor = class(TForm)
    icons: TImageList;
    moDebug: TMemo;
    miHelpSep4: TMenuItem;
    miCheckForUpdates: TMenuItem;
    miManual: TMenuItem;
    miCreateBackground: TMenuItem;
    miCreatePath: TMenuItem;
    miCreateScript: TMenuItem;
    miCreateShader: TMenuItem;
    miCreateFont: TMenuItem;
    miCreateTimeLine: TMenuItem;
    miCreateObject: TMenuItem;
    miCreateRoom: TMenuItem;
    miResourcesSep1: TMenuItem;
    miDefineConstants: TMenuItem;
    miHelpSep1: TMenuItem;
    miRunNormal: TMenuItem;
    miRunDebug: TMenuItem;
    miNewProject: TMenuItem;
    miOpenProject: TMenuItem;
    miRecentProjects: TMenuItem;
    miFileSep1: TMenuItem;
    miSave: TMenuItem;
    miSaveAs: TMenuItem;
    miFileSep2: TMenuItem;
    miPreferences: TMenuItem;
    miMainSite: TMenuItem;
    miFileSep3: TMenuItem;
    miExit: TMenuItem;
    miHelpSep3: TMenuItem;
    miAbout: TMenuItem;
    miWiki: TMenuItem;
    miForums: TMenuItem;
    miHelpSep2: TMenuItem;
    miOpenProjectFolder: TMenuItem;
    miCreateSprite: TMenuItem;
    miCreateSound: TMenuItem;
    mnMain: TMainMenu;
    miFile: TMenuItem;
    miEdit: TMenuItem;
    miHelp: TMenuItem;
    miResources: TMenuItem;
    miRun: TMenuItem;
    pnSplitCover: TPanel;
    psMain: TPairSplitter;
    psMainLeft: TPairSplitterSide;
    psMainRight: TPairSplitterSide;
    tbMain: TToolBar;
    tbRun: TToolButton;
    tbDebug: TToolButton;
    tbSep1: TToolButton;
    tbCreateSprite: TToolButton;
    tbCreateSound: TToolButton;
    tbCreateBackground: TToolButton;
    tbCreatePath: TToolButton;
    tbCreateScript: TToolButton;
    tbCreateShader: TToolButton;
    tbCreateFont: TToolButton;
    tbCreateTimeline: TToolButton;
    tbCreateObject: TToolButton;
    tbCreateRoom: TToolButton;
    tbNewProject: TToolButton;
    tbSep2: TToolButton;
    tmrLazyLoad: TTimer;
    tmrCleanup: TTimer;
    tvMain: TTreeView;
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure miAboutClick(Sender: TObject);
    procedure miCreateBackgroundClick(Sender: TObject);
    procedure miCreateFontClick(Sender: TObject);
    procedure miCreateObjectClick(Sender: TObject);
    procedure miCreatePathClick(Sender: TObject);
    procedure miCreateRoomClick(Sender: TObject);
    procedure miCreateScriptClick(Sender: TObject);
    procedure miCreateShaderClick(Sender: TObject);
    procedure miCreateSoundClick(Sender: TObject);
    procedure miCreateSpriteClick(Sender: TObject);
    procedure miCreateTimeLineClick(Sender: TObject);
    procedure miDebugClick(Sender: TObject);
    procedure miDefineConstantsClick(Sender: TObject);
    procedure miRunNormalClick(Sender: TObject);
    procedure tbCreateBackgroundClick(Sender: TObject);
    procedure tbCreateFontClick(Sender: TObject);
    procedure tbCreateObjectClick(Sender: TObject);
    procedure tbCreatePathClick(Sender: TObject);
    procedure tbCreateRoomClick(Sender: TObject);
    procedure tbCreateScriptClick(Sender: TObject);
    procedure tbCreateShaderClick(Sender: TObject);
    procedure tbCreateSoundClick(Sender: TObject);
    procedure tbCreateSpriteClick(Sender: TObject);
    procedure tbCreateTimelineClick(Sender: TObject);
    procedure tbDebugClick(Sender: TObject);
    procedure tbRunClick(Sender: TObject);
    procedure tmrCleanupTimer(Sender: TObject);
    procedure tmrLazyLoadTimer(Sender: TObject);
    procedure tvMainClick(Sender: TObject);
    procedure tvMainCollapsed(Sender: TObject; Node: TTreeNode);
    procedure tvMainCustomDrawItem(Sender: TCustomTreeView; Node: TTreeNode;
      State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure tvMainDblClick(Sender: TObject);
    procedure tvMainEditing(Sender: TObject; Node: TTreeNode;
      var AllowEdit: Boolean);
    function NodeWithName(parentNode: TTreeNode; searchName: string): boolean;
    procedure CreateSprite();
    procedure ShowSprite(node: TTreeNode);
    procedure CreateSound();
    procedure ShowSound(node: TTreeNode);
    procedure CreateBackground();
    procedure ShowBackground(node: TTreeNode);
    procedure CreatePath();
    procedure ShowPath(node: TTreeNode);
    procedure CreateScript();
    procedure ShowScript(node: TTreeNode);
    procedure CreateShader();
    procedure ShowShader(node: TTreeNode);
    procedure CreateFont();
    procedure ShowFont(node: TTreeNode);
    procedure CreateTimeLine();
    procedure ShowTimeLine(node: TTreeNode);
    procedure CreateObject();
    procedure ShowObject(node: TTreeNode);
    procedure CreateRoom();
    procedure ShowRoom(node: TTreeNode);
    procedure tvMainExpanded(Sender: TObject; Node: TTreeNode);
    procedure tvMainMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer
      );
    procedure tvMainMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmEditor: TfrmEditor;

implementation

{$R *.lfm}

var
  spriteForms: TList;
  soundForms: TList;
  backgroundForms: TList;
  pathForms: TList;
  scriptForms: TList;
  shaderForms: TList;
  fontForms: TList;
  timelineForms: TList;
  objectForms: TList;
  roomForms: TList;

{ TfrmEditor }

// TODO: Better caching system for forms. User settable limits. Allows for fast-reopening since
// the form will not need to be created again. Default of 20-25 for each resource type?
// When Show<Resource> is called it should look for a form which is not currently visible and re-use from this pool
// Do not precreate the forms in the pool! Just SetLength up to that limit. Cleanup timer should clean up
// closed forms higher than the pool limit as they are closed.

// Minimum form dimensions (sprite)

// TODO: window -> cascade. Displays all open forms prettily in case some get lost or something.
// TODO for script editor set options to show close buttons (linux only?)
// todo: option for using one window for code or creating new window each time.

// todo: need to do a more careful examination of where pointers / pass by reference might be needed

function TfrmEditor.NodeWithName(parentNode: TTreeNode; searchName: string): boolean;
var
  i: integer = 0;
begin
  Result := false;
  while i < parentNode.Count do
  begin
    if parentNode.Items[i].Text = searchName then
    begin
      Result := true;
      i := parentNode.Count;
    end;
    inc(i);
  end;
end;

procedure TfrmEditor.ShowSprite(node: TTreeNode);
var
  i: integer = 0;
  found: boolean = false;
  myForm: TfrmSprite;
  mySprite: pSprite;
begin
  myForm := nil;

  // Check if sprite is already open and show it
  while i < spriteForms.Count do
  begin
    if TfrmSprite(spriteForms.Items[i]).node = node then
    begin
      myForm := TfrmSprite(spriteForms.Items[i]);
      found := true;
      i := spriteForms.Count;
    end;
    inc(i);
  end;
  // Add new form and load if not already open.
  if not(found) then
  begin
    myForm := TfrmSprite.Create(nil);
    spriteForms.Add(myForm);
  end;

  // Fill out form properties
  mySprite := GetSpriteFromName(node.Text);
  myForm.node := node;

  myForm.txtName.Text := node.Text;
  myForm.originalName := node.Text;

  myForm.txtX.Text := IntToStr(mySprite^.origin.x);
  myForm.txtY.Text := IntToStr(mySprite^.origin.y);
  myForm.chkPrecise.Checked := mySprite^.usePreciseCollisions;

  myForm.Show;
end;

procedure TfrmEditor.CreateSprite();
var
  node: TTreeNode;
  myForm: TfrmSprite;
  i: integer = 0;
  nodeName: string;
begin
  nodeName := 'sprite'+inttostr(i);
  while NodeWithName(TN_SPRITES, nodeName) do
  begin
    inc(i);
    nodeName := 'sprite'+inttostr(i);
  end;
  node := tvMain.Items.AddChild(TN_SPRITES, nodeName);
  tvMain.Selected := node;

  myForm := TfrmSprite.Create(nil);
  myForm.txtName.Text := nodeName;
  myForm.txtName.SelectAll;
  myForm.node := node;
  myForm.originalName := nodeName;
  spriteForms.Add(myForm);

  AddSprite(node);

  myForm.Show;
end;

procedure TfrmEditor.CreateSound();
var
  node: TTreeNode;
  myForm: TfrmSound;
  i: integer = 0;
  nodeName: string;
begin
  nodeName := 'sound'+inttostr(i);
  while NodeWithName(TN_SOUNDS, nodeName) do
  begin
    inc(i);
    nodeName := 'sound'+inttostr(i);
  end;
  node := tvMain.Items.AddChild(TN_SOUNDS, nodeName);
  node.ImageIndex := 5;
  node.SelectedIndex := 5;
  tvMain.Selected := node;

  myForm := TfrmSound.Create(nil);
  myForm.txtName.Text := nodeName;
  myForm.txtName.SelectAll;
  myForm.node := node;
  myForm.originalName := nodeName;
  soundForms.Add(myForm);

  AddSound(node);

  myForm.Show;
end;

procedure TfrmEditor.ShowSound(node: TTreeNode);
var
  i: integer = 0;
  found: boolean = false;
  myForm: TfrmSound;
  mySound: pSound;
begin
  myForm := nil;
  // Check if sound is already open and show it
  while i < soundForms.Count do
  begin
    if TfrmSound(soundForms.Items[i]).node = node then
    begin
      myForm := TfrmSound(soundForms.Items[i]);
      found := true;
      i := soundForms.Count;
    end;
    inc(i);
  end;
  // Add new form and load if not already open.
  if not(found) then
  begin
    myForm := TfrmSound.Create(nil);
    soundForms.Add(myForm);
  end;

  myForm.txtName.Text := node.Text;
  myForm.originalName := node.Text;
  myForm.node := node;

  myForm.Show;
end;

procedure TfrmEditor.CreateBackground();
var
  node: TTreeNode;
  myForm: TfrmBackground;
  i: integer = 0;
  nodeName: string;
begin
  nodeName := 'background'+inttostr(i);
  while NodeWithName(TN_BACKGROUNDS, nodeName) do
  begin
    inc(i);
    nodeName := 'background'+inttostr(i);
  end;
  node := tvMain.Items.AddChild(TN_BACKGROUNDS, nodeName);
  tvMain.Selected := node;

  myForm := TfrmBackground.Create(nil);
  myForm.txtName.Text := nodeName;
  myForm.txtName.SelectAll;
  myForm.node := node;
  myForm.originalName := nodeName;
  backgroundForms.Add(myForm);

  AddBackground(node);

  myForm.Show;
end;

procedure TfrmEditor.ShowBackground(node: TTreeNode);
var
  i: integer = 0;
  found: boolean = false;
  myForm: TfrmBackground;
  myBackground: pBackground;
begin
  myForm := nil;
  // Check if background is already open and show it
  while i < backgroundForms.Count do
  begin
    if TfrmBackground(backgroundForms.Items[i]).node = node then
    begin
      myForm := TfrmBackground(backgroundForms.Items[i]);
      found := true;
      i := backgroundForms.Count;
    end;
    inc(i);
  end;
  // Add new form and load if not already open.
  if not(found) then
  begin
    myForm := TfrmBackground.Create(nil);
    backgroundForms.Add(myForm);
  end;

  myForm.txtName.Text := node.Text;
  myForm.originalName := node.Text;
  myForm.node := node;

  myForm.Show;
end;

procedure TfrmEditor.CreatePath();
var
  node: TTreeNode;
  myForm: TfrmPath;
  i: integer = 0;
  nodeName: string;
begin
  nodeName := 'path'+inttostr(i);
  while NodeWithName(TN_PATHS, nodeName) do
  begin
    inc(i);
    nodeName := 'path'+inttostr(i);
  end;
  node := tvMain.Items.AddChild(TN_PATHS, nodeName);
  node.ImageIndex := 7;
  node.SelectedIndex := 7;
  tvMain.Selected := node;

  myForm := TfrmPath.Create(nil);
  myForm.txtName.Text := nodeName;
  myForm.txtName.SelectAll;
  myForm.node := node;
  myForm.originalName := nodeName;
  pathForms.Add(myForm);

  AddPath(node);

  myForm.Show;
end;

procedure TfrmEditor.ShowPath(node: TTreeNode);
var
  i: integer = 0;
  found: boolean = false;
  myForm: TfrmPath;
  myPath: pPath;
begin
  myForm := nil;
  // Check if path is already open and show it
  while i < pathForms.Count do
  begin
    if TfrmPath(pathForms.Items[i]).node = node then
    begin
      myForm := TfrmPath(pathForms.Items[i]);
      found := true;
      i := pathForms.Count;
    end;
    inc(i);
  end;
  // Add new form and load if not already open.
  if not(found) then
  begin
    myForm := TfrmPath.Create(nil);
    pathForms.Add(myForm);
  end;

  myForm.txtName.Text := node.Text;
  myForm.originalName := node.Text;
  myForm.node := node;

  myForm.Show;
end;

procedure TfrmEditor.CreateScript();
var
  node: TTreeNode;
  myForm: TfrmScript;
  i: integer = 0;
  nodeName: string;
begin
  nodeName := 'script'+inttostr(i);
  while NodeWithName(TN_SCRIPTS, nodeName) do
  begin
    inc(i);
    nodeName := 'script'+inttostr(i);
  end;
  node := tvMain.Items.AddChild(TN_SCRIPTS, nodeName);
  node.ImageIndex := 8;
  node.SelectedIndex := 8;
  tvMain.Selected := node;

  myForm := TfrmScript.Create(nil);
  myForm.txtName.Text := nodeName;
  myForm.txtName.SelectAll;
  myForm.node := node;
  myForm.originalName := nodeName;
  scriptForms.Add(myForm);

  AddScript(node);

  myForm.Show;
end;

procedure TfrmEditor.ShowScript(node: TTreeNode);
var
  i: integer = 0;
  found: boolean = false;
  myForm: TfrmScript;
  myScript: pScript;
begin
  myForm := nil;
  // Check if script is already open and show it
  while i < scriptForms.Count do
  begin
    if TfrmScript(scriptForms.Items[i]).node = node then
    begin
      myForm := TfrmScript(scriptForms.Items[i]);
      found := true;
      i := scriptForms.Count;
    end;
    inc(i);
  end;
  // Add new form and load if not already open.
  if not(found) then
  begin
    myForm := TfrmScript.Create(nil);
    scriptForms.Add(myForm);
  end;

  myForm.txtName.Text := node.Text;
  myForm.originalName := node.Text;
  myForm.node := node;

  myForm.Show;
end;

procedure TfrmEditor.CreateShader();
var
  node: TTreeNode;
  myForm: TfrmShader;
  i: integer = 0;
  nodeName: string;
begin
  nodeName := 'shader'+inttostr(i);
  while NodeWithName(TN_SHADERS, nodeName) do
  begin
    inc(i);
    nodeName := 'shader'+inttostr(i);
  end;
  node := tvMain.Items.AddChild(TN_SHADERS, nodeName);
  node.ImageIndex := 9;
  node.SelectedIndex := 9;
  tvMain.Selected := node;

  myForm := TfrmShader.Create(nil);
  myForm.txtName.Text := nodeName;
  myForm.txtName.SelectAll;
  myForm.node := node;
  myForm.originalName := nodeName;
  shaderForms.Add(myForm);

  AddShader(node);

  myForm.Show;
end;

procedure TfrmEditor.ShowShader(node: TTreeNode);
var
  i: integer = 0;
  found: boolean = false;
  myForm: TfrmShader;
  myShader: pShader;
begin
  myForm := nil;
  // Check if shader is already open and show it
  while i < shaderForms.Count do
  begin
    if TfrmShader(shaderForms.Items[i]).node = node then
    begin
      myForm := TfrmShader(shaderForms.Items[i]);
      found := true;
      i := shaderForms.Count;
    end;
    inc(i);
  end;
  // Add new form and load if not already open.
  if not(found) then
  begin
    myForm := TfrmShader.Create(nil);
    shaderForms.Add(myForm);
  end;

  myForm.txtName.Text := node.Text;
  myForm.originalName := node.Text;
  myForm.node := node;

  myForm.Show;
end;

procedure TfrmEditor.CreateFont();
var
  node: TTreeNode;
  myForm: TfrmFont;
  i: integer = 0;
  nodeName: string;
begin
  nodeName := 'font'+inttostr(i);
  while NodeWithName(TN_FONTS, nodeName) do
  begin
    inc(i);
    nodeName := 'font'+inttostr(i);
  end;
  node := tvMain.Items.AddChild(TN_FONTS, nodeName);
  node.ImageIndex := 10;
  node.SelectedIndex := 10;
  tvMain.Selected := node;

  myForm := TfrmFont.Create(nil);
  myForm.txtName.Text := nodeName;
  myForm.txtName.SelectAll;
  myForm.node := node;
  myForm.originalName := nodeName;
  fontForms.Add(myForm);

  AddFont(node);

  myForm.Show;
end;

procedure TfrmEditor.ShowFont(node: TTreeNode);
var
  i: integer = 0;
  found: boolean = false;
  myForm: TfrmFont;
  myFont: pFont;
begin
  myForm := nil;
  // Check if font is already open and show it
  while i < fontForms.Count do
  begin
    if TfrmFont(fontForms.Items[i]).node = node then
    begin
      myForm := TfrmFont(fontForms.Items[i]);
      found := true;
      i := fontForms.Count;
    end;
    inc(i);
  end;
  // Add new form and load if not already open.
  if not(found) then
  begin
    myForm := TfrmFont.Create(nil);
    fontForms.Add(myForm);
  end;

  myForm.txtName.Text := node.Text;
  myForm.originalName := node.Text;
  myForm.node := node;

  myForm.Show;
end;

procedure TfrmEditor.CreateTimeLine();
var
  node: TTreeNode;
  myForm: TfrmTimeline;
  i: integer = 0;
  nodeName: string;
begin
  nodeName := 'timeline'+inttostr(i);
  while NodeWithName(TN_TIME_LINES, nodeName) do
  begin
    inc(i);
    nodeName := 'timeline'+inttostr(i);
  end;
  node := tvMain.Items.AddChild(TN_TIME_LINES, nodeName);
  node.ImageIndex := 11;
  node.SelectedIndex := 11;
  tvMain.Selected := node;

  myForm := TfrmTimeline.Create(nil);
  myForm.txtName.Text := nodeName;
  myForm.txtName.SelectAll;
  myForm.node := node;
  myForm.originalName := nodeName;
  timelineForms.Add(myForm);

  AddTimeline(node);

  myForm.Show;
end;

procedure TfrmEditor.ShowTimeLine(node: TTreeNode);
var
  i: integer = 0;
  found: boolean = false;
  myForm: TfrmTimeline;
  myTimeLine: pTimeLine;
begin
  myForm := nil;
  // Check if timeline is already open and show it
  while i < timelineForms.Count do
  begin
    if TfrmTimeline(timelineForms.Items[i]).node = node then
    begin
      myForm := TfrmTimeline(timelineForms.Items[i]);
      found := true;
      i := timelineForms.Count;
    end;
    inc(i);
  end;
  // Add new form and load if not already open.
  if not(found) then
  begin
    myForm := TfrmTimeline.Create(nil);
    timelineForms.Add(myForm);
  end;

  myForm.txtName.Text := node.Text;
  myForm.originalName := node.Text;
  myForm.node := node;

  myForm.Show;
end;

procedure TfrmEditor.ShowObject(node: TTreeNode);
var
  i: integer = 0;
  found: boolean = false;
  myForm: TfrmObject;
  myObject: pObject;
begin
  myForm := nil;
  // Check if already open and show it
  while i < objectForms.Count do
  begin
    if TfrmObject(objectForms.Items[i]).node = node then
    begin
      myForm := TfrmObject(objectForms.Items[i]);
      found := true;
      i := objectForms.Count;
    end;
    inc(i);
  end;
  // Add new form and load if not already open.
  if not(found) then
  begin
    myForm := TfrmObject.Create(nil);
    objectForms.Add(myForm);
  end;

  myForm.txtName.Text := node.Text;
  myForm.originalName := node.Text;
  myForm.node := node;

  myForm.Show;
end;

procedure TfrmEditor.CreateObject();
var
  node: TTreeNode;
  myForm: TfrmObject;
  i: integer = 0;
  nodeName: string;
begin
  nodeName := 'object'+inttostr(i);
  while NodeWithName(TN_OBJECTS, nodeName) do
  begin
    inc(i);
    nodeName := 'object'+inttostr(i);
  end;
  node := tvMain.Items.AddChild(TN_OBJECTS, nodeName);
  tvMain.Selected := node;

  myForm := TfrmObject.Create(nil);
  myForm.txtName.Text := nodeName;
  myForm.txtName.SelectAll;
  myForm.node:= node;
  myForm.originalName := nodeName;
  objectForms.Add(myForm);

  AddObject(node);

  myForm.Show;
end;

procedure TfrmEditor.CreateRoom();
var
  node: TTreeNode;
  myForm: TfrmRoom;
  i: integer = 0;
  nodeName: string;
begin
  nodeName := 'room'+inttostr(i);
  while NodeWithName(TN_ROOMS, nodeName) do
  begin
    inc(i);
    nodeName := 'room'+inttostr(i);
  end;
  node := tvMain.Items.AddChild(TN_ROOMS, nodeName);
  node.ImageIndex := 13;
  node.SelectedIndex := 13;
  tvMain.Selected := node;

  myForm := TfrmRoom.Create(nil);
  myForm.txtName.Text := nodeName;
  myForm.txtName.SelectAll;
  myForm.node := node;
  myForm.originalName := nodeName;
  roomForms.Add(myForm);

  AddRoom(node);

  myForm.Show;
end;

procedure TfrmEditor.ShowRoom(node: TTreeNode);
var
  i: integer = 0;
  found: boolean = false;
  myForm: TfrmRoom;
  myRoom: pRoom;
begin
  myForm := nil;
  // Check if room is already open and show it
  while i < roomForms.Count do
  begin
    if TfrmRoom(roomForms.Items[i]).node = node then
    begin
      myForm := TfrmRoom(roomForms.Items[i]);
      found := true;
      i := roomForms.Count;
    end;
    inc(i);
  end;
  // Add new form and load if not already open.
  if not(found) then
  begin
    myForm := TfrmRoom.Create(nil);
    roomForms.Add(myForm);
  end;

  myForm.txtName.Text := node.Text;
  myForm.originalName := node.Text;
  myForm.node := node;

  myForm.Show;
end;

procedure TfrmEditor.tvMainExpanded(Sender: TObject; Node: TTreeNode);
begin
  Node.StateIndex := 3;
end;

procedure TfrmEditor.tvMainMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  n: TTreeNode;
begin
  n := tvMain.GetNodeAt(X, Y);
  if n = nil then
  begin
    tvMain.Cursor := crDefault;
  end else
  begin
    tvMain.Cursor := crHandPoint;
  end;
end;

procedure TfrmEditor.tvMainMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  n: TTreeNode;
begin
  // Reset selection if a node was not clicked
  n := tvMain.GetNodeAt(X, Y);
  if n = nil then
  begin
    tvMain.Selected := nil;
  end else
  begin
    tvMain.Selected := n;
  end;

  if Button = mbRight then
  begin
    // TODO: show menu
  end;
end;

procedure TfrmEditor.miDebugClick(Sender: TObject);
begin
  // Create form that requires user's attention
  {newform := TfrmObject.Create(nil);
  try
    newform.ShowModal;
  finally
    newform.Free;
  end;}
end;

procedure TfrmEditor.miDefineConstantsClick(Sender: TObject);
begin
  frmConstants.Show;
end;

procedure TfrmEditor.miRunNormalClick(Sender: TObject);
begin

end;

procedure TfrmEditor.tbCreateBackgroundClick(Sender: TObject);
begin
  CreateBackground;
end;

procedure TfrmEditor.tbCreateFontClick(Sender: TObject);
begin
  CreateFont;
end;

procedure TfrmEditor.tbCreateObjectClick(Sender: TObject);
begin
  CreateObject;
end;

procedure TfrmEditor.tbCreatePathClick(Sender: TObject);
begin
  CreatePath;
end;

procedure TfrmEditor.tbCreateRoomClick(Sender: TObject);
begin
  CreateRoom;
end;

procedure TfrmEditor.tbCreateScriptClick(Sender: TObject);
begin
  CreateScript;
end;

procedure TfrmEditor.tbCreateShaderClick(Sender: TObject);
begin
  CreateShader;
end;

procedure TfrmEditor.tbCreateSoundClick(Sender: TObject);
begin
  CreateSound;
end;

procedure TfrmEditor.tbCreateSpriteClick(Sender: TObject);
begin
  CreateSprite;
end;

procedure TfrmEditor.tbCreateTimelineClick(Sender: TObject);
begin
  CreateTimeLine;
end;

procedure TfrmEditor.tbDebugClick(Sender: TObject);
begin

end;

procedure TfrmEditor.tbRunClick(Sender: TObject);

begin

end;

// Cleanup closed resource forms and then clean up the list
procedure TfrmEditor.tmrCleanupTimer(Sender: TObject);
var
  i: integer = 0;
begin
  while i < spriteForms.Count do
  begin
    if not(TfrmSprite(spriteForms[i]).IsVisible) then
    begin
      TfrmSprite(spriteForms[i]).Free;
      spriteForms.Delete(i);
      i := -1;
    end;
    inc(i);
  end;
  i := 0;
  while i < soundForms.Count do
  begin
    if not(TfrmSound(soundForms[i]).IsVisible) then
    begin
      TfrmSound(soundForms[i]).Free;
      soundForms.Delete(i);
      i := -1;
    end;
    inc(i);
  end;
  i := 0;
  while i < backgroundForms.Count do
  begin
    if not(TfrmBackground(backgroundForms[i]).IsVisible) then
    begin
      TfrmBackground(backgroundForms[i]).Free;
      backgroundForms.Delete(i);
      i := -1;
    end;
    inc(i);
  end;
  i := 0;
  while i < pathForms.Count do
  begin
    if not(TfrmPath(pathForms[i]).IsVisible) then
    begin
      TfrmPath(pathForms[i]).Free;
      pathForms.Delete(i);
      i := -1;
    end;
    inc(i);
  end;
  i := 0;
  while i < scriptForms.Count do
  begin
    if not(TfrmScript(scriptForms[i]).IsVisible) then
    begin
      TfrmScript(scriptForms[i]).Free;
      scriptForms.Delete(i);
      i := -1;
    end;
    inc(i);
  end;
  i := 0;
  while i < shaderForms.Count do
  begin
    if not(TfrmShader(shaderForms[i]).IsVisible) then
    begin
      TfrmShader(shaderForms[i]).Free;
      shaderForms.Delete(i);
      i := -1;
    end;
    inc(i);
  end;
  i := 0;
  while i < fontForms.Count do
  begin
    if not(TfrmFont(fontForms[i]).IsVisible) then
    begin
      TfrmFont(fontForms[i]).Free;
      fontForms.Delete(i);
      i := -1;
    end;
    inc(i);
  end;
  i := 0;
  while i < timelineForms.Count do
  begin
    if not(TfrmTimeline(timelineForms[i]).IsVisible) then
    begin
      TfrmTimeline(timelineForms[i]).Free;
      timelineForms.Delete(i);
      i := -1;
    end;
    inc(i);
  end;
  i := 0;
  while i < objectForms.Count do
  begin
    if not(TfrmObject(objectForms[i]).IsVisible) then
    begin
      TfrmObject(objectForms[i]).Free;
      objectForms.Delete(i);
      i := -1;
    end;
    inc(i);
  end;
  i := 0;
  while i < roomForms.Count do
  begin
    if not(TfrmRoom(roomForms[i]).IsVisible) then
    begin
      TfrmRoom(roomForms[i]).Free;
      roomForms.Delete(i);
      i := -1;
    end;
    inc(i);
  end;
end;

procedure TfrmEditor.tmrLazyLoadTimer(Sender: TObject);
begin
  tmrLazyLoad.Enabled := false;
end;

procedure TfrmEditor.tvMainClick(Sender: TObject);
begin

end;

procedure TfrmEditor.tvMainCollapsed(Sender: TObject; Node: TTreeNode);
begin
  Node.StateIndex := 2;
end;

// REQUIRES disabling tvoThemedDraw in Options
// We are custom drawing for the classic GM feel. Mmm.
// TODO
procedure TfrmEditor.tvMainCustomDrawItem(Sender: TCustomTreeView;
  Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
var
  DrawRect: TRect;
begin
  DefaultDraw := true;
  {if cdsSelected in State then
  begin
    tvMain.Font.Color := clRed;
  end;}

  if not DefaultDraw then
  begin
    with tvMain do
    begin
      DrawRect := Node.DisplayRect(false);
      Brush.Style := bsClear;
      Font.Style := [];

      // Draw arrows
      {DrawRect.Left := DrawRect.Left + (Node.Level * Indent);
      DrawButton(DrawRect, Node);}

      // Draw icon
      {DrawRect.Left := DrawRect.Left + Indent + FButtonSize;
      DrawImage(DrawRect, Node.ImageIndex);}

      // Draw text
      // Bold the main resource nodes
      if Node = TN_SPRITES then
        Font.Style := [fsBold];
      if Node = TN_SOUNDS then
        Font.Style := [fsBold];
      if Node = TN_BACKGROUNDS then
        Font.Style := [fsBold];
      if Node = TN_PATHS then
        Font.Style := [fsBold];
      if Node = TN_SCRIPTS then
        Font.Style := [fsBold];
      if Node = TN_SHADERS then
        Font.Style := [fsBold];
      if Node = TN_FONTS then
        Font.Style := [fsBold];
      if Node = TN_TIME_LINES then
        Font.Style := [fsBold];
      if Node = TN_OBJECTS then
        Font.Style := [fsBold];
      if Node = TN_ROOMS then
        Font.Style := [fsBold];

      // Draw the text
      DrawRect.Left := DrawRect.Left + Images.Width;
      Canvas.TextOut(DrawRect.Left, DrawRect.Top, Node.Text);
    end;
  end;
end;

procedure TfrmEditor.tvMainDblClick(Sender: TObject);
var
  selectedNode, parentNode: TTreeNode;
  mousePos: TPoint;
begin
  mousePos := Mouse.CursorPos;
  mousePos := ScreenToClient(mousePos);
  mousePos.y := mousePos.y - tbMain.Height;
  selectedNode := tvMain.GetNodeAt(mousePos.x, mousePos.y);
  if not(selectedNode = nil) then
  begin
    if selectedNode = TN_GGS then
    begin
      exit;
    end;
    if selectedNode = TN_CONSTANTS then
    begin
      frmConstants.Show;
      exit;
    end;

    // If we double click the main resource folder, do nothing
    if selectedNode.Level = 0 then
      exit;

    tvMain.Selected := selectedNode;
    // TODO: move this to an "OpenResource(node: TTreeNode)" ?
    parentNode := selectedNode.GetParentNodeOfAbsoluteLevel(0);
    if parentNode = TN_SPRITES then
    begin
      ShowSprite(selectedNode);
    end;
    if parentNode = TN_SOUNDS then
    begin
      ShowSound(SelectedNode);
    end;
    if parentNode = TN_BACKGROUNDS then
    begin
      ShowBackground(selectedNode);
    end;
    if parentNode = TN_PATHS then
    begin
      ShowPath(selectedNode);
    end;
    if parentNode = TN_SCRIPTS then
    begin
      ShowScript(selectedNode);
    end;
    if parentNode = TN_SHADERS then
    begin
      ShowShader(selectedNode);
    end;
    if parentNode = TN_FONTS then
    begin
      ShowFont(selectedNode);
    end;
    if parentNode = TN_TIME_LINES then
    begin
      ShowTimeLine(selectedNode);
    end;
    if parentNode = TN_OBJECTS then
    begin
      ShowObject(selectedNode);
    end;
    if parentNode = TN_ROOMS then
    begin
      ShowRoom(selectedNode);
    end;
  end;
end;

procedure TfrmEditor.FormCreate(Sender: TObject);
begin
  // Grab references to root treeview elements
  TN_SPRITES := tvMain.Items[TV_SPRITES];
  TN_SOUNDS := tvMain.Items[TV_SOUNDS];
  TN_BACKGROUNDS := tvMain.Items[TV_BACKGROUNDS];
  TN_PATHS := tvMain.Items[TV_PATHS];
  TN_SCRIPTS := tvMain.Items[TV_SCRIPTS];
  TN_SHADERS := tvMain.Items[TV_SHADERS];
  TN_FONTS := tvMain.Items[TV_FONTS];
  TN_TIME_LINES := tvMain.Items[TV_TIME_LINES];
  TN_OBJECTS := tvMain.Items[TV_OBJECTS];
  TN_ROOMS := tvMain.Items[TV_ROOMS];
  TN_GGS := tvMain.Items[TV_GGS];
  TN_CONSTANTS := tvMain.Items[TV_CONSTANTS];

  spriteForms := TList.Create;
  soundForms := TList.Create;
  backgroundForms := TList.Create;
  pathForms := TList.Create;
  scriptForms := TList.Create;
  shaderForms := TList.Create;
  fontForms := TList.Create;
  timelineForms := TList.Create;
  objectForms := TList.Create;
  roomForms := TList.Create;

  frmEditor.WindowState := wsMaximized;
end;

procedure TfrmEditor.miAboutClick(Sender: TObject);
begin
  frmAbout.Show;
end;

procedure TfrmEditor.miCreateBackgroundClick(Sender: TObject);
begin
  CreateBackground;
end;

procedure TfrmEditor.miCreateFontClick(Sender: TObject);
begin
  CreateFont;
end;

procedure TfrmEditor.miCreateObjectClick(Sender: TObject);
begin
  CreateObject;
end;

procedure TfrmEditor.miCreatePathClick(Sender: TObject);
begin
  CreatePath;
end;

procedure TfrmEditor.miCreateRoomClick(Sender: TObject);
begin
  CreateRoom;
end;

procedure TfrmEditor.miCreateScriptClick(Sender: TObject);
begin
  CreateScript;
end;

procedure TfrmEditor.miCreateShaderClick(Sender: TObject);
begin
  CreateShader;
end;

procedure TfrmEditor.miCreateSoundClick(Sender: TObject);
begin
  CreateSound;
end;

procedure TfrmEditor.miCreateSpriteClick(Sender: TObject);
begin
  CreateSprite;
end;

procedure TfrmEditor.miCreateTimeLineClick(Sender: TObject);
begin
  CreateTimeLine;
end;

// TODO: check saved status
procedure TfrmEditor.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  i: integer = 0;
begin
  // Clean up forms
  while i < spriteForms.Count do
  begin
    TfrmSprite(spriteForms[i]).Free;
    inc(i);
  end;
  i := 0;
  while i < soundForms.Count do
  begin
    TfrmSound(soundForms[i]).Free;
    inc(i);
  end;
  i := 0;
  while i < backgroundForms.Count do
  begin
    TfrmBackground(backgroundForms[i]).Free;
    inc(i);
  end;
  i := 0;
  while i < pathForms.Count do
  begin
    TfrmPath(pathForms[i]).Free;
    inc(i);
  end;
  i := 0;
  while i < scriptForms.Count do
  begin
    TfrmScript(scriptForms[i]).Free;
    inc(i);
  end;
  i := 0;
  while i < shaderForms.Count do
  begin
    TfrmShader(shaderForms[i]).Free;
    inc(i);
  end;
  i := 0;
  while i < fontForms.Count do
  begin
    TfrmFont(fontForms[i]).Free;
    inc(i);
  end;
  i := 0;
  while i < timelineForms.Count do
  begin
    TfrmTimeline(timelineForms[i]).Free;
    inc(i);
  end;
  i := 0;
  while i < objectForms.Count do
  begin
    TfrmObject(objectForms[i]).Free;
    inc(i);
  end;
  i := 0;
  while i < roomForms.Count do
  begin
    TfrmRoom(roomForms[i]).Free;
    inc(i);
  end;

  // Clean up arrays of forms
  spriteForms.Free;
  soundForms.Free;
  backgroundForms.Free;
  pathForms.Free;
  scriptForms.Free;
  shaderForms.Free;
  fontForms.Free;
  timelineForms.Free;
  objectForms.Free;
  roomForms.Free;

  // Clean up resource records
  i := 0;
  while i < Length(pSprites) do
  begin
    Dispose(pSprites[i]);
    Inc(i);
  end;
  i := 0;
  while i < Length(pSounds) do
  begin
    Dispose(pSounds[i]);
    Inc(i);
  end;
  i := 0;
  while i < Length(pBackgrounds) do
  begin
    Dispose(pBackgrounds[i]);
    Inc(i);
  end;
  i := 0;
  while i < Length(pPaths) do
  begin
    Dispose(pPaths[i]);
    Inc(i);
  end;
  i := 0;
  while i < Length(pScripts) do
  begin
    Dispose(pScripts[i]);
    Inc(i);
  end;
  i := 0;
  while i < Length(pShaders) do
  begin
    Dispose(pShaders[i]);
    Inc(i);
  end;
  i := 0;
  while i < Length(pFonts) do
  begin
    Dispose(pFonts[i]);
    Inc(i);
  end;
  i := 0;
  while i < Length(pTimeLines) do
  begin
    Dispose(pTimeLines[i]);
    Inc(i);
  end;
  i := 0;
  while i < Length(pObjects) do
  begin
    Dispose(pObjects[i]);
    Inc(i);
  end;
  i := 0;
  while i < Length(pRooms) do
  begin
    Dispose(pRooms[i]);
    Inc(i);
  end;

  CanClose := true;
end;

// TODO: do check to see if they used right click menu... double clicking should not begin editing.
procedure TfrmEditor.tvMainEditing(Sender: TObject; Node: TTreeNode;
  var AllowEdit: Boolean);
begin
  if Node.Level < 1 then
  begin

  end;
  AllowEdit := false;
end;

end.

