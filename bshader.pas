unit bshader;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  StdCtrls, globals;

type

  { TfrmShader }

  TfrmShader = class(TForm)
    lblName: TLabel;
    txtName: TEdit;
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormShow(Sender: TObject);
    procedure UpdateTitle();
    procedure txtNameChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    node: TTreeNode;
    originalName: string;
  end;

implementation

{$R *.lfm}

{ TfrmShader }
procedure TfrmShader.UpdateTitle();
begin
  Caption := SShaderProperties + ': ' + txtName.Text;
end;

procedure TfrmShader.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  myShader: pShader;
begin
  myShader := GetShaderFromName(originalName);
  myShader^.formPos.Left := Left;
  myShader^.formPos.Top := Top;
  myShader^.formPos.Right := Right;
  myShader^.formPos.Bottom := bottom;
  CanClose := true;
end;

procedure TfrmShader.FormShow(Sender: TObject);
var
  myShader: pShader;
begin
  myShader := GetShaderFromName(originalName);
  // -1, -1 is the default value, if we have it don't override our default position
  if not(myShader^.formPos.Left = -1) and not(myShader^.formPos.Top = -1) and 
     not(myShader^.formPos.Right = -1) and not(myShader^.formPos.Bottom = -1) then
  begin
    Left := myShader^.formPos.Left;
    Top := myShader^.formPos.Top;
    Right := myShader^.formPos.Right;
    Bottom := myShader^.formPos.Bottom;
  end;
end;

procedure TfrmShader.txtNameChange(Sender: TObject);
begin
  UpdateTitle;
end;

end.

