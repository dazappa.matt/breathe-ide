unit bpath;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  StdCtrls, Buttons, globals;

type

  { TfrmPath }

  TfrmPath = class(TForm)
    btnOK: TBitBtn;
    lblName: TLabel;
    txtName: TEdit;
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormShow(Sender: TObject);
    procedure UpdateTitle();
    procedure txtNameChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    node: TTreeNode;
    originalName: string;
  end;

implementation

{$R *.lfm}

{ TfrmPath }
procedure TfrmPath.UpdateTitle();
begin
  Caption := SPathProperties + ': ' + txtName.Text;
end;

procedure TfrmPath.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  myPath: pPath;
begin
  myPath := GetPathFromName(originalName);
  myPath^.formPos.Left := Left;
  myPath^.formPos.Top := Top;
  myPath^.formPos.Right := Right;
  myPath^.formPos.Bottom := bottom;
  CanClose := true;
end;

procedure TfrmPath.FormShow(Sender: TObject);
var
  myPath: pPath;
begin
  myPath := GetPathFromName(originalName);
  // -1, -1 is the default value, if we have it don't override our default position
  if not(myPath^.formPos.Left = -1) and not(myPath^.formPos.Top = -1) and 
     not(myPath^.formPos.Right = -1) and not(myPath^.formPos.Bottom = -1) then
  begin
    Left := myPath^.formPos.Left;
    Top := myPath^.formPos.Top;
    Right := myPath^.formPos.Right;
    Bottom := myPath^.formPos.Bottom;
  end;
end;

procedure TfrmPath.txtNameChange(Sender: TObject);
begin
  UpdateTitle;
end;

end.

