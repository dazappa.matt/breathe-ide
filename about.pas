unit about;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, lclintf;

type

  { TfrmAbout }

  TfrmAbout = class(TForm)
    grpBy: TGroupBox;
    grpContributors: TGroupBox;
    imgDonate: TImage;
    lblLicense: TLabel;
    lblTitle: TLabel;
    lblDescription: TLabel;
    lblBy: TLabel;
    lblWeb: TLabel;
    lblDonate: TLabel;
    lblRelease: TLabel;
    procedure lblDonateMouseEnter(Sender: TObject);
    procedure lblDonateMouseLeave(Sender: TObject);
    procedure lblLicenseMouseEnter(Sender: TObject);
    procedure lblLicenseMouseLeave(Sender: TObject);
    procedure lblWebMouseEnter(Sender: TObject);
    procedure lblWebMouseLeave(Sender: TObject);
    procedure lblWebMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmAbout: TfrmAbout;

implementation

{$R *.lfm}

{ TfrmAbout }

procedure TfrmAbout.lblWebMouseEnter(Sender: TObject);
begin
  lblWeb.Font.Style := [fsUnderline];
end;

procedure TfrmAbout.lblDonateMouseEnter(Sender: TObject);
begin
  lblDonate.Font.Style := [fsUnderline];
end;

procedure TfrmAbout.lblDonateMouseLeave(Sender: TObject);
begin
  lblDonate.Font.Style := [];
end;

procedure TfrmAbout.lblLicenseMouseEnter(Sender: TObject);
begin
  lblLicense.Font.Style := [fsUnderline];
end;

procedure TfrmAbout.lblLicenseMouseLeave(Sender: TObject);
begin
  lblLicense.Font.Style := [];
end;

procedure TfrmAbout.lblWebMouseLeave(Sender: TObject);
begin
  lblWeb.Font.Style := [];
end;

procedure TfrmAbout.lblWebMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  OpenURL('https://matthews.world');
end;

end.

