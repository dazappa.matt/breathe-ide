unit bobject;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons,
  StdCtrls, ComCtrls, ExtCtrls, Menus, globals;

type

  { TfrmObject }

  TfrmObject = class(TForm)
    btnOk: TBitBtn;
    btnNewSprite: TButton;
    btnEditSprite: TButton;
    btnAddEvent: TButton;
    btnDeleteEvent: TButton;
    btnChangeEvent: TButton;
    btnViewParent: TButton;
    btnViewMask: TButton;
    chkVisible: TCheckBox;
    chkPersistent: TCheckBox;
    chkSolid: TCheckBox;
    chkUsesPhysics: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    icons: TImageList;
    Image1: TImage;
    Image10: TImage;
    Image11: TImage;
    Image12: TImage;
    Image13: TImage;
    Image14: TImage;
    Image15: TImage;
    Image16: TImage;
    Image18: TImage;
    Image19: TImage;
    Image2: TImage;
    Image20: TImage;
    Image21: TImage;
    Image22: TImage;
    Image23: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    Image8: TImage;
    Image9: TImage;
    mnMaskSame: TMenuItem;
    mnParentNone: TMenuItem;
    mnSpriteNone: TMenuItem;
    popSprite: TPopupMenu;
    popParent: TPopupMenu;
    popMask: TPopupMenu;
    txtName: TEdit;
    txtSprite: TEdit;
    txtDepth: TEdit;
    txtParent: TEdit;
    txtMask: TEdit;
    grpSprite: TGroupBox;
    lblName: TLabel;
    lblDepth: TLabel;
    lblEvents: TLabel;
    lblActions: TLabel;
    lblChildren: TLabel;
    lvEvents: TListView;
    lvActions: TListView;
    lvChildren: TListView;
    pgctrlMain: TPageControl;
    btnSelectSprite: TSpeedButton;
    btnSelectParent: TSpeedButton;
    btnSelectMask: TSpeedButton;
    pgMove: TTabSheet;
    pgMain1: TTabSheet;
    pgMain2: TTabSheet;
    pgControl: TTabSheet;
    pgScore: TTabSheet;
    pgExtra: TTabSheet;
    pgDraw: TTabSheet;
    procedure btnSelectMaskClick(Sender: TObject);
    procedure btnSelectParentClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormShow(Sender: TObject);
    procedure txtMaskClick(Sender: TObject);
    procedure txtParentClick(Sender: TObject);
    procedure txtSpriteClick(Sender: TObject);
    procedure UpdateTitle();
    procedure btnSelectSpriteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure txtNameChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    node: TTreeNode;
    originalName: string;
  end;

implementation

{$R *.lfm}

{ TfrmObject }
procedure TfrmObject.UpdateTitle();
begin
  Caption := SObjectProperties + ': ' + txtName.Text;
end;

procedure TfrmObject.txtSpriteClick(Sender: TObject);
begin
  popSprite.PopUp;
end;

procedure TfrmObject.txtParentClick(Sender: TObject);
begin
  popParent.PopUp;
end;

procedure TfrmObject.btnSelectParentClick(Sender: TObject);
begin
  popParent.PopUp;
end;

procedure TfrmObject.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  myObject: pObject;
begin
  myObject := GetObjectFromName(originalName);
  myObject^.formPos.Left := Left;
  myObject^.formPos.Top := Top;
  myObject^.formPos.Right := Right;
  myObject^.formPos.Bottom := bottom;
  CanClose := true;
end;

procedure TfrmObject.FormShow(Sender: TObject);
var
  myObject: pObject;
begin
  myObject := GetObjectFromName(originalName);
  // -1, -1 is the default value, if we have it don't override our default position
  if not(myObject^.formPos.Left = -1) and not(myObject^.formPos.Top = -1) and 
     not(myObject^.formPos.Right = -1) and not(myObject^.formPos.Bottom = -1) then
  begin
    Left := myObject^.formPos.Left;
    Top := myObject^.formPos.Top;
    Right := myObject^.formPos.Right;
    Bottom := myObject^.formPos.Bottom;
  end;
end;

procedure TfrmObject.btnSelectMaskClick(Sender: TObject);
begin
  popMask.PopUp;
end;

procedure TfrmObject.txtMaskClick(Sender: TObject);
begin
  popMask.PopUp;
end;

procedure TfrmObject.FormCreate(Sender: TObject);
begin
  //btnSelectParent.Glyph.Assign(btnSelectSprite.Glyph);
  //btnSelectMask.Glyph.Assign(btnSelectSprite.Glyph);
  //btnOk.Glyph.Assign(btnSelectSprite.Glyph)
end;

procedure TfrmObject.txtNameChange(Sender: TObject);
begin
  UpdateTitle;
end;

procedure TfrmObject.btnSelectSpriteClick(Sender: TObject);
begin
  popSprite.PopUp;
end;

end.

