unit imageloader;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  TImageLoaderThread = class(TThread)
  private
    done: boolean;
    success: boolean;
  public
    function IsDone(): boolean;
    function Successful(): boolean;
    procedure Abort;
    procedure Execute; override;
  end;

var
  imageLoading: boolean = false;
  ImageLoaderThread: TThread;

implementation

function TImageLoaderThread.IsDone(): boolean;
begin
  Result := done;
end;

function TImageLoaderThread.Successful(): boolean;
begin
  Result := success;
end;

procedure TImageLoaderThread.Abort;
begin
  success := false;
  done := true;
end;

procedure TImageLoaderThread.Execute;
begin
  done := false;
  success := false;
  Sleep(5000);

  done := true;
  success := true;
end;

end.

