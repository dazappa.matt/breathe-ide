unit broom;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  StdCtrls, globals;

type

  { TfrmRoom }

  TfrmRoom = class(TForm)
    lblName: TLabel;
    txtName: TEdit;
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormShow(Sender: TObject);
    procedure UpdateTitle();
    procedure txtNameChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    node: TTreeNode;
    originalName: string;
  end;

implementation

{$R *.lfm}

{ TfrmRoom }
procedure TfrmRoom.UpdateTitle();
begin
  Caption := SRoomProperties + ': ' + txtName.Text;
end;

procedure TfrmRoom.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  myRoom: pRoom;
begin
  myRoom := GetRoomFromName(originalName);
  myRoom^.formPos.Left := Left;
  myRoom^.formPos.Top := Top;
  myRoom^.formPos.Right := Right;
  myRoom^.formPos.Bottom := bottom;
  CanClose := true;
end;

procedure TfrmRoom.FormShow(Sender: TObject);
var
  myRoom: pRoom;
begin
  myRoom := GetRoomFromName(originalName);
  // -1, -1 is the default value, if we have it don't override our default position
  if not(myRoom^.formPos.Left = -1) and not(myRoom^.formPos.Top = -1) and 
     not(myRoom^.formPos.Right = -1) and not(myRoom^.formPos.Bottom = -1) then
  begin
    Left := myRoom^.formPos.Left;
    Top := myRoom^.formPos.Top;
    Right := myRoom^.formPos.Right;
    Bottom := myRoom^.formPos.Bottom;
  end;
end;

procedure TfrmRoom.txtNameChange(Sender: TObject);
begin
  UpdateTitle;
end;

end.

